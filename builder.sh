#!/bin/bash
#

DEVICE="$1"
BUILD_TYPE="$2"
UPLOAD_HOST="$3"
ROM_DIR=

# User vars
export BUILD_HOSTNAME=
export BUILD_USERNAME=

# Token
BOT_TOKEN=
PD_KEY=

# Chat ID
CHAT_ID=
TOPIC_ID=

# ROM var 
BUILD_PACKAGE="PixelOS_$DEVICE-14.0-*"
BUILD_LOG="$ROM_DIR/out/error.log"
OUT_DIR="$ROM_DIR/out/target/product/$DEVICE"
UPLOAD_DIR="pixelos"


# Send Telegram message
push_msg() {
    curl -s -X POST "https://api.telegram.org/bot$BOT_TOKEN/sendMessage" -d "chat_id=$CHAT_ID&parse_mode=html&text=$1" -d "reply_to_message_id=$TOPIC_ID"
}

# Builder
builder() {
    local start_time=$(date +%s)
    push_msg "🛠 CI | PixelOS (14)%0ADevice: $DEVICE%0ALunch flavor: aosp_$DEVICE-$BUILD_TYPE"

    . build/envsetup.sh
    lunch aosp_$DEVICE-$BUILD_TYPE
    mka bacon -j$(nproc --all)

    local end_time=$(date +%s)
    build_status $start_time $end_time
}

# Build time
count_build_time() {
    local start_time=$1
    local end_time=$2

    local duration=$((end_time - start_time))
    local seconds=$((duration % 60))
    local minutes=$((duration / 60 % 60))
    local hours=$((duration / 3600))

    local time_message=""
    [ $hours -gt 0 ] && time_message="${hours}h "
    [ $minutes -gt 0 ] && time_message="${time_message}${minutes}min "
    [ $seconds -gt 0 ] && time_message="${time_message}${seconds}s "

    echo "$time_message"
}

# Build status
build_status() {
    local start_time=$1
    local end_time=$2
    build_time=$(count_build_time $start_time $end_time)

    if [ -e $OUT_DIR/$BUILD_PACKAGE.zip ]; then
        push_msg "build completed sucessfully%0ATotal time elapsed: <b>$build_time</b>"
        uploader
    else
        push_msg "build failed"
        curl -F chat_id="$CHAT_ID"  -F reply_to_message_id="$TOPIC_ID" -F document=@"$BUILD_LOG" "https://api.telegram.org/bot$BOT_TOKEN/sendDocument"
    fi
}

# Upload ROM package

pdrain_uploader() {
    local FILE_PATH="$1"
    local FILE_NAME="${FILE_PATH##*/}"
    
    local response
    response=$(curl -# -F "name=$FILE_NAME" -F "file=@$FILE_PATH" -u :$PD_KEY "https://pixeldrain.com/api/file")

    local FILE_ID
    FILE_ID=$(echo "$response" | grep -Po '(?<="id":")[^"]*')

    push_msg "Uploaded to PixelDrain%0A1. $BUILD_ID | <b>SHA256: </b>$SHA256SUM_CHECK%0A2. Download: https://pixeldrain.com/u/$FILE_ID"
}

uploader() {

    # Build vars
    BUILD_ID=$(basename $(ls $OUT_DIR/$BUILD_PACKAGE.zip))
    SHA256SUM_CHECK=$(basename $(sha256sum $OUT_DIR/$BUILD_PACKAGE.zip))

    # Upload 
	case $UPLOAD_HOST in

        # Google Drive
		gdrive)
			rclone copy $OUT_DIR/$BUILD_PACKAGE.zip gd:ROMs/$UPLOAD_DIR
			push_msg "Uploaded to Gdrive%0A1. $BUILD_ID | <b>SHA256: </b>$SHA256SUM_CHECK"
			;;

        # OneDrive
		onedrive)
			rclone copy $OUT_DIR/$BUILD_PACKAGE.zip od:ROMs/$UPLOAD_DIR
			push_msg "Uploaded to OneDrive%0A1. $BUILD_ID | <b>SHA256: </b>$SHA256SUM_CHECK"
			;;

        # PixelDrain
		pixeldrain)
			pdrain_uploader $OUT_DIR/$BUILD_ID
			;;

		*)
			echo "Upload host not defined"
			;;
	esac
}

builder
